var modCalendar = function (eventOptions, options, calendarOptions) {
    var optionDefaults = {
        refetchTime: 10000,
        calendarSelector: "#calendar",
        errorSelector: "#loading_error",
        loadingSelector: "#loading"
    };
    options = Object.assign({}, optionDefaults, options);
    var eventDefaults = {
        'url': null,
        'data': null,
        'render': render,
        'updateUrl': null
    };
    eventOptions = Object.assign({}, eventDefaults, eventOptions);
    var calendarDefaults = {
        'navLinks': true,
        'defaultView': "month",
        'editable': eventOptions.updateUrl !== null,
        'eventDurationEditable': false,
        'eventLimit': true,
        'defaultTimedEventDuration': "00:20:00",
        'header': {},
    };
    calendarOptions = Object.assign({
        'events': {
            'url': eventOptions.url,
            'data': eventOptions.data,
            'error': function () {
                $(options.errorSelector).show();
            }
        },
        'loading': function (v) {
            if (!v) {
                $(options.loadingSelector).hide();
            }
        },
        'eventDrop': eventDrop,
        'eventDragStart': function (event, delta, revertFunc) {
            dragging = true;
        },
        'eventDragStop': function (event, delta, revertFunc) {
            dragging = false;
        },
        'eventRender': eventOptions.render
    }, calendarDefaults, calendarOptions);

    Object.assign(calendarOptions.header, {
        'left': "prev, next, today",
        'center': "title",
        'right': "month, agendaWeek, agendaDay, listDay"
    });

    function render(event, el) {
        var t = el.find(".fc-title");
        var s = (typeof event.prefix !== "undefined") ? event.prefix : "";
        if (t.length < 1) {
            t = el.find(".fc-list-item-title a");
            s += t.text();
        } else {
            s += t.text();
            s = "<span href='#' title='" + s.replace(/'/g, "&apos;") + "'>" + s + "</span>";
        }
        t.html(s);
    }

    function eventDrop(event, delta, revertFunc) {
        var offset = delta.asSeconds();
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState === XMLHttpRequest.DONE) {
                if (xmlhttp.status === 200) {
                    var resp = JSON.parse(this.responseText);
                    if (resp.success !== true) {
                        revertFunc();
                    }
                } else {
                    console.log("Request error", xmlhttp);
                    revertFunc();
                }
            }
        };
        xmlhttp.open("PUT", eventOptions.updateUrl + "?id=" + event.eventId + "&offset=" + offset, true);
        xmlhttp.setRequestHeader("Content-type", "application/json");
        xmlhttp.send();
    }

    var calendar = $(options.calendarSelector);
    var dragging = false;

    function refetch() {
        if (!dragging) {
            calendar.fullCalendar("refetchEvents");
        }
    }

    calendar.fullCalendar(calendarOptions);

    setInterval(refetch, options.refetchTime);
};
