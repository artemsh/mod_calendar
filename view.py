from flask import render_template, jsonify, request
from flask.views import View


class CalendarView(View):
    def __init__(self, calendar):
        super(CalendarView, self).__init__()
        self.calendar = calendar


class IndexView(CalendarView):
    methods = ["GET"]
    DEFAULT_TEMPLATE = "calendar.html"

    @property
    def template_name(self):
        return self.DEFAULT_TEMPLATE

    def dispatch_request(self):
        c = self.calendar
        return render_template(c.template_path + self.template_name, **c.args())


class EventsView(CalendarView):
    methods = ["GET"]

    def dispatch_request(self):
        c = self.calendar
        return jsonify([x() for x in c.data(request.args)])


class UpdateView(CalendarView):
    methods = ["PUT"]

    def _get_model(self, id_):
        raise NotImplementedError

    def _do_update(self, model, offset):
        raise NotImplementedError

    def _fail_handler(self, e):
        pass

    def dispatch_request(self):
        success = False
        try:
            id_ = int(request.args.get('id'))
            m = self._get_model(id_)
            if m is not None:
                success = self._do_update(m, int(request.args.get('offset')))
        except (ValueError, TypeError) as e:
            self._fail_handler(e)
        return jsonify({'success': success})
