import collections
from flask import Blueprint, url_for

from .view import IndexView, EventsView


class ModCalendar(object):
    EP_INDEX = "index"
    EP_EVENTS = "events"
    EP_UPDATE = "update"

    def __init__(self, data_provider, mod_name="mod_calendar", template_path=None, app=None, page_args=None,
                 blueprint=None, decorators=None,
                 index_view=IndexView, events_view=EventsView, update_view=None):
        self.app = None
        self._data_provider = data_provider
        self._mod_name = mod_name
        self._blueprint = blueprint
        self._template_path = template_path if template_path is not None else self.mod_name
        if not self._template_path.endswith("/"):
            self._template_path += "/"
        self._page_args = page_args or {}
        self.index_view = index_view
        self.events_view = events_view
        self.update_view = update_view
        self.decorators = decorators
        if app is not None:
            self.init_app(app)

    def _apply_decorators(self, fn):
        if not self.decorators:
            return fn
        # TODO: do not move to init or have setter for self.decorators
        if not isinstance(self.decorators, collections.Iterable):
            self.decorators = [self.decorators]
        for d in self.decorators:
            fn = d(fn)
        return fn

    def ep(self, ep):
        return "{}.{}".format(self.mod_name, ep)

    def init_app(self, app):
        self.app = app
        if self._blueprint is None:
            self._blueprint = Blueprint(self._mod_name, __name__, template_folder='templates')
            self.app.register_blueprint(self._blueprint, url_prefix="/" + self._mod_name)
        self.app.jinja_env.globals[self._mod_name] = self
        if not hasattr(app, 'extensions'):
            app.extensions = {}
        if self._mod_name in app.extensions:
            raise RuntimeError("{} is already initialized.".format(self._mod_name))
        app.extensions[self._mod_name] = self
        index_func = self._apply_decorators(self.index_view.as_view(self.ep(self.EP_INDEX), self))
        app.add_url_rule("/{}/".format(self._mod_name), view_func=index_func)
        app.add_url_rule("/{}/events".format(self._mod_name),
                         view_func=self._apply_decorators(self.events_view.as_view(self.ep(self.EP_EVENTS), self)))
        if self.update_view:
            app.add_url_rule("/{}/update".format(self._mod_name),
                             view_func=self._apply_decorators(self.update_view.as_view(self.ep(self.EP_UPDATE), self)))

    @property
    def mod_name(self):
        return self._mod_name

    @property
    def template_path(self):
        return self._template_path

    def args(self):
        page_args = self._page_args(self) if callable(self._page_args) else self._page_args
        path = {
            'jquery': "js/jquery.min.js",
            'jquery-ui': "js/jquery-ui.min.js",
            'moment': "js/moment.min.js",
        }
        path.update(page_args.get('path', {}))
        ret = {
            'calendar': self,
        }
        ret.update(page_args)
        ret['path'] = {x: url_for("static", filename=y) for x, y in path.items()}
        return ret

    def data(self, args):
        return self._data_provider(args)
