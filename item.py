from datetime import datetime

from flask import current_app


class CalendarItem(object):
    def __init__(self, start, end, title, params=None):
        self.start = start
        self.end = end
        self.title = title
        self.params = params or {}

    @classmethod
    def _convert_datetime(cls, dt):
        if not isinstance(dt, datetime):
            dt = datetime.fromtimestamp(dt, current_app.config['TIMEZONE'])
        return dt.isoformat()

    def __call__(self):
        ret = {
            'start': self._convert_datetime(self.start),
            'title': self.title,
        }
        if self.end is not None:
            ret['end'] = self._convert_datetime(self.end)
        ret.update(self.params)
        return ret
